#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/internet-module.h"

using namespace ns3;

// Constants
const double tsifs = 0.000001;
const double tdifs = 0.000005;
const double tslot = 0.000002;
const double CWmin = 15;
const double m = 8;

// Global Variables
int retryBitSetCountRx = 0;
int retryBitUnsetCountRx = 0;
std::vector<double> tphysicalvec, tpayloadvec, tackvec;
int retryBitSetCountTx = 0;
int retryBitUnsetCountTx = 0;
std::vector<double> tphysicalvecTx, tpayloadvecTx, tackvecTx;


// Trace Sink
void MonitorSniffRx (Ptr<const Packet> packet,
                     uint16_t channelFreqMhz,
                     WifiTxVector txVector,
                     MpduInfo aMpdu,
                     SignalNoiseDbm signalNoise){
    // Count Number of packets with retry bit set and unset
    WifiMacHeader header;
    packet->PeekHeader(header);
    if(header.IsRetry()){
        // if retry bit set
        retryBitSetCountRx++;
    }
    else{
        // if retry bit unset
        retryBitUnsetCountRx++;
    }
    
    YansWifiPhy phy;
    // ACK Package
    if(header.IsAck()){
        // find transmission time for packet
        
        int size = packet->GetSize();
        double time = phy.CalculateTxDuration(size, txVector, channelFreqMhz).GetSeconds();
        tackvec.push_back(time);
    }
    else{
        // find tx time for both packet and payload
        int packetsize = packet->GetSize();
        double time = phy.CalculateTxDuration(packetsize, txVector, channelFreqMhz).GetSeconds();
        tphysicalvec.push_back(time);
        //std::cout<<time<<"\t";
        
        // remove header and trailer
        Packet packet2(*packet);
        WifiMacHeader throwheader;
        packet2.RemoveHeader(throwheader);
        WifiMacTrailer trailer;
        packet2.RemoveTrailer(trailer);
        int payloadsize = packet2.GetSize();
        double payloadtime = phy.CalculateTxDuration(payloadsize, txVector, channelFreqMhz).GetSeconds();
        tpayloadvec.push_back(payloadtime);
    }
}

// Trace Sink
void MonitorSniffTx (Ptr<const Packet> packet,
                     uint16_t channelFreqMhz,
                     WifiTxVector txVector,
                     MpduInfo aMpdu
                     /*,SignalNoiseDbm signalNoise*/){
    // Count Number of packets with retry bit set and unset
    WifiMacHeader header;
    packet->PeekHeader(header);
    if(header.IsRetry()){
        // if retry bit set
        retryBitSetCountTx++;
    }
    else{
        // if retry bit unset
        retryBitUnsetCountTx++;
    }
    
    YansWifiPhy phy;
    // ACK Package
    if(header.IsAck()){
        // find transmission time for packet
        
        int size = packet->GetSize();
        double time = phy.CalculateTxDuration(size, txVector, channelFreqMhz).GetSeconds();
        tackvecTx.push_back(time);
    }
    else{
        // find tx time for both packet and payload
        int packetsize = packet->GetSize();
        double time = phy.CalculateTxDuration(packetsize, txVector, channelFreqMhz).GetSeconds();
        tphysicalvecTx.push_back(time);
        //std::cout<<time<<"\t";
        
        // remove header and trailer
        Packet packet2(*packet);
        WifiMacHeader throwheader;
        packet2.RemoveHeader(throwheader);
        WifiMacTrailer trailer;
        packet2.RemoveTrailer(trailer);
        int payloadsize = packet2.GetSize();
        double payloadtime = phy.CalculateTxDuration(payloadsize, txVector, channelFreqMhz).GetSeconds();
        tpayloadvecTx.push_back(payloadtime);
    }
}


double calculateBandwidth(int R, int D, double tpayload, double tphysical, double tack, int n){
    // retry ratio
    double r0 = R/(double)(R + D);
    
    //collision probability
    double pc = 0;
    for(auto i = 1; i <= m; i++){
        pc += std::pow(r0, i);
    }
    
    // channel utilization ratio
    double S = (2 * (1 - pc)/(2 - pc)) * (tpayload / (tphysical + tsifs + tack + tdifs + (CWmin * tslot / (n + 1))));
    
    // available throughput
    double Snewstation = S / (double)(n + 1);
    double theta_new_station = Snewstation * 11;
    return theta_new_station;
}

int main(int argc, char* argv[]){
    // logging
    NS_LOG_COMPONENT_DEFINE("project");
    
    // configuration
    int seconds = 15;
    
    //CLI arguments
    CommandLine cmd;    
    cmd.AddValue("seconds","Number of seconds to run simulation", seconds);
    cmd.Parse(argc, argv);
    
    // number of wifi devices 
    // including new station
    // excluding AP
    int nWifi = 5;

    // Create nodes
    NodeContainer wifiStaNodes;
    wifiStaNodes.Create(nWifi);
    // Declare new station and AP
    NodeContainer wifiAP;
    wifiAP.Create(1);   // AP
    NodeContainer newStation = wifiStaNodes.Get(0);

    // create channel
    YansWifiChannelHelper channel = YansWifiChannelHelper::Default();
    YansWifiPhyHelper phy = YansWifiPhyHelper::Default();
    phy.SetChannel(channel.Create());

    // Create MAC Layer
    WifiHelper wifi;
    std::string phyMode ("DsssRate11Mbps");
    wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode",StringValue (phyMode),
                                "ControlMode",StringValue (phyMode));
    // Set standard to 802.11b
    wifi.SetStandard (WIFI_PHY_STANDARD_80211b);

    WifiMacHelper mac;
    Ssid ssid = Ssid("ACN");
    // Station Node,
    // QoS false by default
    // Infrastructure BSS
    // ActiveProbing false
    mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid),
            "ActiveProbing", BooleanValue(false));


    // Install stations
    NetDeviceContainer staDevices;
    staDevices = wifi.Install(phy, mac, wifiStaNodes);

    // Setup AP
    mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));
    NetDeviceContainer apDevice;
    apDevice = wifi.Install(phy, mac, wifiAP);

    // Every node is stationary
    MobilityHelper mobility;
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(wifiAP);
    mobility.Install(wifiStaNodes);

    // Install Internet Stack
    InternetStackHelper stack;
    stack.Install(wifiAP);
    stack.Install(wifiStaNodes);

    // Assign IPs
    Ipv4AddressHelper address;
    address.SetBase("10.0.0.0", "255.255.255.0");
    Ipv4InterfaceContainer apinterface = address.Assign(apDevice);
    Ipv4InterfaceContainer stainterface = address.Assign(staDevices);

    // Install Browser emulating applications
    // Server is station 1
    Ipv4Address serverAddress = stainterface.GetAddress(1);
    // create server helper
    ThreeGppHttpServerHelper serverHelper(serverAddress);
    // install http server
    ApplicationContainer serverApps = serverHelper.Install(wifiStaNodes.Get(1));
    Ptr<ThreeGppHttpServer> httpServer = serverApps.Get(0)->GetObject<ThreeGppHttpServer> ();

    // Setup Http server parameters
    PointerValue httpVarPtr;
    httpServer->GetAttribute("Variables", httpVarPtr);
    Ptr<ThreeGppHttpVariables> httpVariables = httpVarPtr.Get<ThreeGppHttpVariables>();
    httpVariables->SetMainObjectSizeMean(1024 * 1024 * 10);  //10 MB
    httpVariables->SetMainObjectSizeStdDev(1024 * 1024 * 4);  //4 MB

    // Setup Http clients
    ThreeGppHttpClientHelper clientHelper(serverAddress);
    // Install http clients
    for(int i = 2; i < nWifi; i++){
        ApplicationContainer clientApp = clientHelper.Install(wifiStaNodes.Get(i));
        Ptr<ThreeGppHttpClient> httpClient = clientApp.Get(0)->GetObject<ThreeGppHttpClient>();
    }

    // stop simulator after given seconds
    Simulator::Stop(Seconds(seconds));
    
    // Connect to trace source
    Config::ConnectWithoutContext ("/NodeList/0/DeviceList/*/Phy/MonitorSnifferRx", MakeCallback (&MonitorSniffRx));
    Config::ConnectWithoutContext ("/NodeList/0/DeviceList/*/Phy/MonitorSnifferTx", MakeCallback (&MonitorSniffTx));
    
    // run simulator
    Simulator::Run();
    Simulator::Destroy();
    
    // Print Rx Bandwidth
    double tpayload = 0;
    for(auto value : tpayloadvec){
        tpayload += value;
    }
    tpayload /= tpayloadvec.size();
    
    double tphysical = 0;
    for(auto value : tphysicalvec){
        tphysical += value;
    }
    tphysical /= tphysicalvec.size();
    
    double tack = 0;
    for(auto value : tackvec){
        tack += value;
    }
    tack /= tackvec.size();
    
    std::cout<<"Rx:\t"<<calculateBandwidth(retryBitSetCountRx, retryBitUnsetCountRx,
     tpayload, tphysical, tack, nWifi)<<" Mbps\n";
     
    // Print Tx Bandwidth
    double tpayloadTx = 0;
    for(auto value : tpayloadvecTx){
        tpayloadTx += value;
    }
    tpayloadTx /= tpayloadvecTx.size();
    
    double tphysicalTx = 0;
    for(auto value : tphysicalvecTx){
        tphysicalTx += value;
    }
    tphysicalTx /= tphysicalvecTx.size();
    
    double tackTx = 0;
    for(auto value : tackvecTx){
        tackTx += value;
    }
    tackTx /= tackvecTx.size();
    
    std::cout<<"Tx:\t"<<calculateBandwidth(retryBitSetCountTx, retryBitUnsetCountTx,
     tpayloadTx, tphysicalTx, tackTx, nWifi)<<" Mbps\n";
    
    return 0;
}
